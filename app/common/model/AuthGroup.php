<?php
namespace app\common\model;

use think\Model;
use think\Db;
use think\Loader;

class AuthGroup extends Model
{
    /**
     * 获取用户组详情信息
     *
     * @params $group_id (int)
     * @return array
     */
    public static function getInfo($group_id)
    {
        return Db::name('auth_group')->where('id',$group_id)->find();
    }

    /**
     * 保存组信息
     *
     * @params data
     * @return int
     */
    public static function store($_data)
    {
        $data['name'] = $_data['name'];
        $data['remark'] = $_data['remark'];
        $validate = Loader::validate('AuthRuleValidate');
        $result = $validate->scene('add')->check($data);
        if(!$result){
            return $validate->getError();
        }
        return Db::name('auth_group')->insertGetId($data);
    }

    /**
     * 更新组信息
     *
     * @params data
     * @return int
    */
    public static function updateData($_data)
    {
        $data['id'] = $_data['id'];
        $data['name'] = $_data['name'];
        $data['remark'] = $_data['remark'];
        $validate = Loader::validate('AuthRuleValidate');
        $result = $validate->scene('add')->check($data);
        if(!$result){
            return $validate->getError();
        }
        return Db::name('auth_group')->update($data);
    }

    /**
     * 获取用户组列表
     *
     * @return array
     */
    public static function getList()
    {
        return Db::name('auth_group')->select();
    }

    /**
     * 删除组
     *
     * @params $group_id
     * @return int
     */
    public static function remove($group_id)
    {
        return Db::name('auth_group')->delete($group_id);
    }

    /**
     * 根据用ID 查询用户组名称
     *
     * @params $group_id int 组ID
     * @return string|bool
     */
    public static function getName($group_id)
    {
        return Db::name('auth_group')->where('id',$group_id)->value('name');
    }
}
