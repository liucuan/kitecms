<?php
namespace app\common\model;

use think\Model;
use think\Db;
use app\common\model\AuthGroup;

class User extends Model
{
    /**
     * 获取用户详情信息
     *
     * @params $uid
     * @return array
     */
    public static function getInfo($uid)
    {
        return Db::name('user')
            ->alias('u')
            ->field('u.*, g.name as group_name')
            ->where('u.uid', $uid)
            ->join('__AUTH_GROUP__ g', 'u.group_id = g.id', 'LEFT')
            ->find();
    }

    /**
     * 根据用户名获取
     *
     * @params $username
     * @return array
     */
    public static function getInfoByName($username)
    {
        return Db::name('user')->where('username', $username)->find();
    }

    /**
     * 获取用户列表
     *
     * @params $map(array),$order(array),$page_size(int),$params(arra)
     * @return array
     */
    public static function getList($map = [], $order = ['id'=>'desc'], $page_size = 2, $params = [])
    {
        return Db::name('user')
            ->alias('u')
            ->field('u.*, g.name as group_name')
            ->whereOr($map)
            ->join('__AUTH_GROUP__ g', 'u.group_id = g.id', 'LEFT')
            ->order($order)
            ->paginate($page_size,false,[
                'type'     => 'bootstrap',
                'var_page' => 'page',
                'query'    =>$params,
            ]);
    }
    
    /**
     * 保存用户信息
     *
     * @params data
     * @return int
     */
    public static function add($data)
    {
        return Db::name('user')->insertGetId($data);
    }

    /**
     * 更新用户信息
     *
     * @params data
     * @return int
     */
    public static function modify($data)
    {
        return Db::name('user')->update($data);
    }

    /**
     * 删除用户信息
     *
     * @params $data array()
     * @return int
     */
    public static function remove($uid)
    {
        return Db::name('user')->delete($uid);
    }
    
    /**
     * 检测用户名是否重复
     *
     * @params map,array('username,email,phone')
     * @return mix
     */
    public static function checkUser($map)
    {
        return Db::name('user')->where($map)->find();
    }

    /**
     * 获取用户UID
     *
     * @params $username string 用户名
     * @return int
     */
    public static function getUid($username)
    {
        return Db::name('user')->where('username', $username)->value('uid');
    }
    
    /**
     * 获取用户名
     *
     * @params $uid int 用户ID
     * @return string
     */
    public static function getUsername($uid)
    {
        return Db::name('user')->where('username', $username)->value('uid');
    }

    /**
     * 获取用户别名
     *
     * @params $uid int 用户ID
     * @return string|boolean
     */
    public static function getNickname($uid)
    {
        return Db::name('user')->where('uid', $uid)->value('nickname');
    }

    /**
     * 设置用户头像
     *
     * @params $uid int 用户ID
     * @params $value string 头像路径
     * @return string|boolean
     */
    public static function setAvatar($uid ,$value)
    {
        return Db::name('user')->where('uid',$uid)->setField('avatar', $value);
    }

    /**
     * 获取用户详情信息
     *
     * @params $username string 用户账号
     * @return array|boolean
     */
    public static function getDetail($username)
    {
        return Db::name('user')->where('username', $username)->find();
    }

    /**
     * 获取用户密码
     *
     * @params $uid int 用户ID
     * @return string
     */
    public static function getPassword($uid)
    {
        return Db::name('user')->where('uid', $uid)->value('password');
    }

    /**
     * 获取用户密码
     *
     * @params $data array 登陆数据
     * @return int
     */
    public static function addLoginLog($data)
    {
        return Db::name('user_login_log')->insertGetId($data);
    }
}