<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Favorite extends Model
{
    /**
     * 收藏的文章列表
     *
     * @params $uid int 用户编号ID
     * @return array
     */
    public static function getArticleList($uid, $limit)
    {
        return $list = Db::name('user_favorite')
            ->alias('f')
            ->field('a.*,f.create_at as collect_at , c.alias as category_alias, c.title as category_name, u.username, u.motto, u.avatar')
            ->join('__ARTICLE__ a', 'a.id = f.collect', 'LEFT')
            ->join('__CATEGORY__ c','a.category_id = c.id','LEFT')
            ->join('__USER__ u', 'a.uid = u.uid', 'LEFT')
            ->where('f.uid', '=', $uid)
            ->where('f.type', '=', 1)
            ->paginate($limit);
    }

    /**
     * 收藏的图书列表
     *
     * @params $uid int 用户编号ID
     * @return array
     */
    public static function getBookList($uid, $limit)
    {
        return $list = Db::name('user_favorite')
            ->alias('f')
            ->field('b.*,f.create_at as collect_at , c.alias as category_alias, c.title as category_name, u.username, u.motto, u.avatar')
            ->join('__BOOK__ b', 'b.id = f.collect', 'LEFT')
            ->join('__CATEGORY__ c','b.category_id = c.id','LEFT')
            ->join('__USER__ u', 'b.uid = u.uid', 'LEFT')
            ->where('f.uid', '=', $uid)
            ->where('f.type', '=', 2)
            ->paginate($limit);
    }
}