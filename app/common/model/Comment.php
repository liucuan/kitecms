<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Comment extends Model
{

    /*
     * 评论详情
     *
     * @params $id int 评论ID
     * @params $limit int 每页数量
     * @return array|boolean
     */
    public static function getInfo($id)
    {
        return Db::name('article_comment')
            ->alias('c')
            ->field('c.*, a.title as article_name, a.image as article_image, u.username as article_username, to.username as to_username, from.username as from_username, from.avatar as from_avatar')
            ->where('c.id', $id)
            ->join('__ARTICLE__ a','a.id = c.article_id','LEFT')
            ->join('__USER__ to', 'c.to_uid = to.uid', 'LEFT')
            ->join('__USER__ from', 'c.from_uid = from.uid', 'LEFT')
            ->join('__USER__ u', 'a.uid = u.uid', 'LEFT')
            ->find();
    }

    /*
     * 评论列表
     *
     * @params $map array 查询条件
     * @params $limit int 每页数量
     * @return array|boolean
     */
    public static function getList($map, $order, $limit)
    {
        return Db::name('article_comment')
            ->alias('c')
            ->field('c.*, a.title as article_name, a.image as article_image, u.username as article_username, to.username as to_username, from.username as from_username, from.avatar as from_avatar')
            ->where($map)
            ->join('__ARTICLE__ a','a.id = c.article_id','LEFT')
            ->join('__USER__ to', 'c.to_uid = to.uid', 'LEFT')
            ->join('__USER__ from', 'c.from_uid = from.uid', 'LEFT')
            ->join('__USER__ u', 'a.uid = u.uid', 'LEFT')
            ->order($order)
            ->paginate($limit);
    }

    /*
     * 评论列表
     *
     * @params $map array 查询条件
     * @params $limit int 每页数量
     * @return array|boolean
     */
    public static function getAll($map, $order, $limit)
    {
        return Db::name('article_comment')
            ->alias('c')
            ->field('c.*, a.title as article_name, a.image as article_image, u.username as article_username, to.username as to_username, from.username as from_username, from.avatar as from_avatar')
            ->where($map)
            ->join('__ARTICLE__ a','a.id = c.article_id','LEFT')
            ->join('__USER__ to', 'c.to_uid = to.uid', 'LEFT')
            ->join('__USER__ from', 'c.from_uid = from.uid', 'LEFT')
            ->join('__USER__ u', 'a.uid = u.uid', 'LEFT')
            ->order($order)
            ->limit($limit)
            ->select();
    }

    /**
     * 创建信息
     *
     * @params $data array
     * @return mix
     */
    public static function add($data)
    {
        return Db::name('article_comment')->insertGetId($data);
    }

    /*
     * 更新信息
     *
     * @params $data array
     * @return mix
     */
    public static function modify($data)
    {
        return Db::name('article_comment')->update($data);
    }

    /*
     * 统计评论条数
     *
     * @params $article_id int 文章ID
     * @return int
     */
    public static function getTotal($article_id)
    {
        return Db::name('article_comment')
            ->where('article_id', '=', $article_id)
            ->where('status', '=', 1)
            ->count();
    }

    /*
     * 审核评论
     *
     * @params $id int 评论ID
     * @params $value int 0 待审 1通过
     * @return mix
     */
    public static function setStatus($id ,$value)
    {
        return Db::name('article_comment')->where('id',$id)->setField('status', $value);
    }

    /*
     * 删除
     *
     * @params $id
     * @return mix
     */
    public static function remove($id)
    {
        return Db::name('article_comment')->delete($id);
    }

    /*
     * 获取喜欢数量
     *
     * @params $id int 评论ID
     * @return int
     */
    public static function getLike($id)
    {
        return Db::name('article_comment')->where('id', $id)->value('like');
    }

    /*
     * 设置喜欢数量
     *
     * @params $id int 评论ID
     * @params $num int 喜欢数量
     * @return int
     */
    public static function setLike($id, $num)
    {
        return Db::name('article_comment')->where('id', $id)->setField('like', $num);
    }
}