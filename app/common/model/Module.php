<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Module extends Model
{
    public static function getList($map = [])
    {
        return Db::name('module')->where($map)->select();
    }
    
    public static function getInfo($id)
    {
        return Db::name('module')->where('id',$id)->find();
    }
    
}