<?php
namespace app\common\model;

use think\Model;
use think\Db;

class Attachment extends Model
{
    /*
     * 获取图片列表
     *
     * param $map 数据库字段 scenic_id , model;
     * return bool
     */
    public static function getList($map)
    {
        return Db::name('image')->where($map)->select();
    }

    /*
     * 获取图片详情
     *
     * param $id int 图片ID
     * return bool
     */
    public static function getInfo($id)
    {
        return Db::name('image')->where($map)->find();
    }

    /*
     * 增加图片
     *
     * param array 数据库字段;
     * return bool
     */
    public static function add($param)
    {
        return Db::name('image')->insert($param);
        
    }
    
    /*
     * 更新图片
     *
     * param $data array 数据库字段;
     * return bool
     */
    public static function edit($data)
    {
        return Db::name('image')->update($data);
    }

    /*
     * 删除图片
     *
     * param $id int 图片ID
     * return bool
     */
    public static function remove($id)
    {
        return Db::name('image')->delete($id);
    }

}