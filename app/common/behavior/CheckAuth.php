<?php
namespace app\common\behavior;

use think\Controller;
use think\Session;
use think\Request;
use app\common\model\AuthRule;

class CheckAuth extends Controller
{
    public function run(&$params)
    {
        $request = Request::instance();
        $map['module']     = strtolower($request->module());
        $map['controller'] = strtolower($request->controller());
        $map['action']     = strtolower($request->action());

        $rule = AuthRule::getInfo($map);
        // 如果该操作方法在数据库中则进行校验权限
        // 凡是加入到rule表中的规则都是需要验证
        if (!empty($rule)) {
            // 判断是否登录
            $user = session('user_auth');
            if (empty($user)) {
                $this->error('请先登录', url('admin/passport/login'));
            }
            // 是否后台用户
            if ($user['state'] != 1) {
                $this->error('非法操作', url('index/member/my'));
            }
            // 登录后验证权限
            $bool = in_array($rule['id'], session::get('user_access'));

            // 拥有权限可以进行操作 || 用户UID等于1(创始管理员)可以进行操作
            if (!$bool && $user['uid'] != 1) {
                return $this->error('无操作权限');
            }
        }
    }
}