<?php
namespace app\common\service;

use app\common\model\Message as MessageModel;
use app\common\service\ServiceBase;

class Message extends ServiceBase
{
    /**
     * 获取用消息详情
     *
     * @params $message_id int 消息ID
     * @return array|boolean
     */
    public function getInfo($message_id)
    {
        $data = MessageModel::getInfo($message_id);
        return $this->formatMessage($data);
    }

    /**
     * 获取用户信息列表
     *
     * @params $uid int 用户ID
     * @return array|boolean
     */
    public function getList($map, $order, $limit)
    {
        $result = MessageModel::getList($map, $order, $limit);
        $data['page'] = $result->render();
        $data['list'] = $this->formatMessage($result);
        return $data;
    }
}