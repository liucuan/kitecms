<?php
namespace app\common\service;

use think\Request;
use app\common\model\Book as BookModel;
use app\common\model\Section as SectionModel;
use app\common\model\Chapter as ChapterModel;
use app\common\service\Chapter as ChapterLogic;
use app\common\model\Category as CategoryModel;
use app\common\service\ServiceBase;

class Book extends ServiceBase
{
    /**
     * 图书信息
     *
     * @params $book_id int 图书ID
     * @return array
     */
    public function getInfo($book_id)
    {
        $chapterLogic = new ChapterLogic();
        // 查询图书信息
        $book = $this->formatBook(BookModel::getInfo($book_id));

        // 遍历图书分卷 和 章节
        if ($book['is_section'] == 1) {
            $section = SectionModel::getAll(['s.book_id' => $book_id], 'id asc');
            $data = [];
            if (!empty($section)) {
                foreach ($section as $v) {
                    $v['chapter'] = $chapterLogic->getAll(['section_id' => $v['id']], 'id asc');
                    array_push($data, $v);
                }
            }
            $book['section'] = $data;
        } else {
            $book['chapter'] = $chapterLogic->getAll(['book_id' => $book_id], 'id asc');
        }

        return $book;
    }

    /**
     * 获取图书列表
     *
     * @params $category_id sting 分类ID
     * @params $order array|sting 排序方式
     * @params $limit int 查询数量
     * @return array
     */
    public function getAll($map, $order, $limit, $params = [])
    {
        // 查询
        $map['b.status'] = 1; // 状态 0待审核, 1已审核
        $list = BookModel::getList($map, $order, $limit, $params = []);
        $data['list'] = $this->formatBook($list);
        $data['page'] = $list->render();

        return $data;
    }

    /**
     * 获取指定栏目图书列表
     *
     * @params $category_id sting 分类ID
     * @params $order array|sting 排序方式
     * @params $limit int 查询数量
     * @return array
     */
    public function getList($category_id, $order, $limit)
    {
        // 如果为空则查询所有分类 不为空查询指定分类 
        // 多个分类格式 '1,2,3'
        if ($category_id != 'null') {
            $map['category_id'] = array('in', $category_id);
        }

        // 查询
        $map['b.status'] = 2; //状态 1待审核, 2已审核
        $list = BookModel::getList($map, $order, $limit);

        return $this->formatBook($list);
    }

    /*
     * 获取分页
     * @params $map        array 查询条件
     * @params $page_size  int   查询数量
     * @params $params     array 分页附带参数
     * @return string
    */
    public function getPage($category_id, $limit)
    {
        // 如果为空则查询所有分类 不为空查询指定分类 
        // 多个分类格式 '1,2,3'
        if ($category_id != 'null') {
            $map['category_id'] = array('in', $category_id);
        }

        // 查询
        $map['status'] = 2; // 状态 1待审核, 2已审核
        $params['category_id'] = $category_id;
        $data = BookModel::getPage($map, $limit);

        return $data;
    }

    /**
     * 获取指定推荐位列表(推荐 热门)
     *
     * @params $category_id array 分类ID
     * @params $recommend int 是否推荐 1
     * @params $hot int 是否热门 1
     * @params $order mix 排序方式
     * @params $limit int 查询数量
     * @return array
     */
    public function getPosition($category_id, $recommend, $hot, $order, $limit)
    {
        // 查询规则 1 分类ID等于null 查询所有分类下图书
        //          2 分类ID等于self 查询当前栏目下图书
        //          3 分类ID等于栏目ID 查询指定分类,查询多个分类，ID用逗号隔开

        // 查询当前栏目下图书
        if ($category_id == 'self') {
            $alias = Request::instance()->param('alias');
            $map['category_id'] = CategoryModel::getID($alias);
        }

        // 查询多个栏目
        if ($category_id != 'null' && $category_id != 'self'){
            $map['category_id'] = array('in', $category_id);
        }

        // 是否推荐
        if ($recommend == 1) {
            $map['recommend'] = 1;
        }

        // 是否热门
        if ($hot == 1) {
            $map['hot'] = 1;
        }

        // 查询限制
        $map['b.status'] = 1; // 状态 0待审核, 1已审核
        $list = BookModel::getAll($map, $order, $limit);
        return $this->formatBook($list);
    }
}