<?php
namespace app\common\service;

use think\Request;
use app\common\model\Comment as CommentModel;
use app\common\service\ServiceBase;

class Comment extends ServiceBase
{

    /**
     * 评论列表
     *
     * @params $article_id int 文章ID
     * @params $limit      int 每页数量
     * @return array
     */
    public function getAll($article_id, $order, $limit)
    {
        if ($article_id != 'null') {
            $map['c.article_id'] = $article_id;
        }

        // 限制条件
        $map['c.status'] = 1; //0待审核 1已审核
        $map['c.parent_id'] = 0; // 只查询评论 不计算回复
        $list = CommentModel::getAll($map, $order, $limit);

        return $this->formatComment($list);
    }

    /**
     * 文章评论列表
     *
     * @params $article_id int 文章ID
     * @return array
     */
    public function getList($article_id, $order, $limit)
    {
        $list = CommentModel::getList(['c.article_id' => $article_id, 'c.status' => 1, 'c.parent_id' => 0], $order, $limit);

        $format_list = $this->formatComment($list);
        $new_list = [];
        if (!empty($format_list)) {
            foreach ($format_list as $v) {
                $child = CommentModel::getAll(['c.parent_id' => $v['id'], 'c.status' => 1], 'c.create_at desc', 99999);
                $v['child'] = $this->formatComment($child);
                $v['child_sum'] = count($child);
                array_push($new_list, $v);
            }
        }

        $data['list'] = $new_list;
        $data['page'] = $list->render();

        return $data;
    }

    /**
     * 我的评论
     *
     * @params $commonts_id int 评论ID
     * @return array
     */
    public function myComment($uid, $limit)
    {
        $list = CommentModel::getList(['to_uid' => $uid], 'id desc', $limit);
        $data['list'] = $this->formatComment($list);
        $data['page'] = $list->render();

        return $data;
    }
}