<?php
namespace app\common\logic;

use think\Model;
use email\Email;
use app\common\model\Config as ConfigModel;

class Message extends Model
{
    /**
     * 发送邮件
     *
     * @params $email sting 收件人邮箱
     * @params $subject sting 邮件标题
     * @params $content sting 发送内容
     * @return bool
     */
    public function sendEmail($email, $subject, $content)
    {
        $config = json_decode(ConfigModel::getValue('mail'), true);

        $mail = new Email();

        // $mail->setServer("smtp.163.com", "XXX@gmail.com", "XXX", 465, true); //到服务器的SSL连接
        //如果不需要到服务器的SSL连接，这样设置服务器：$mail->setServer("smtp.126.com", "XXX@126.com", "XXX");
        if ($config['mail_ssl'] == 1) {
            $mail->setServer($config['mail_host'], $config['mail_user'], $config['mail_pwd'], $config['mail_port'], true);
        } else {
            $mail->setServer($config['mail_host'], $config['mail_user'], $config['mail_pwd']);
        }

        // 发送邮件
        $mail->setFrom($config['mail_user']);
        $mail->setReceiver($email);
        $mail->setMail($subject, $content);
        return $mail->sendMail();
    }
}