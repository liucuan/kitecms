<?php
namespace app\common\logic;

use think\Model;
use think\Loader;
use app\common\validate\ConfigValidate;
use app\common\model\Config as ConfigModel;

class Config extends Model
{
    const CONFIG_EXISTS = '配置项标识已存在';
    const NOT_ALLOW_CHANGE = '系统配置不允许变更KEY或者数据类型';
    const NOT_ALLOW_REMOVE = '系统配置不允许删除';

    /**
     * 创建保存信息
     *
     * @params $data array 数据
     * @return mix
     */
    public function add($data)
    {
        // 数据规则验证
        $validate = Loader::validate('ConfigValidate');
        $result = $validate->scene('add')->check($data);
        if (!$result) {
            return $validate->getError();
        }

        // 创建之前校验key是否存在
        $exsits = ConfigModel::getValue($data['key']);
        if ($exsits) {
            return self::CONFIG_EXISTS;
        }

        return ConfigModel::setConfig($data);
    }

    public function edit($data)
    {
        // 数据规则验证
        $validate = Loader::validate('ConfigValidate');
        $result = $validate->scene('edit')->check($data);
        if (!$result) {
            return $validate->getError();
        }

        $info = ConfigModel::getInfo($data['id']);
        // 校验是否为系统配置
        if ($info['state'] == 1) {
            if ($info['key'] != $data['key'] || $info['type'] != $data['type']) {
                return self::NOT_ALLOW_CHANGE;
            }
        }

        // 如果key值和未提交之前不一致 则校验key是否已经存在
        if ($data['key'] != $info['key']) {
            $exsits = ConfigModel::getValue($data['key']);
            if ($exsits) {
                return self::CONFIG_EXISTS;
            }
        }

        return ConfigModel::edit($data);
    }

    public function remove($id)
    {
        $info = ConfigModel::getInfo($id);
        // 校验是否为系统配置
        if ($info['state'] == 1) {
            return self::NOT_ALLOW_REMOVE;
        }

        return ConfigModel::remove($id);
    }
}