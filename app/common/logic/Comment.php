<?php
namespace app\common\logic;

use think\Model;
use think\Loader;
use app\common\validate\CommentValidate;
use app\common\model\Comment as CommentModel;

class Comment extends Model
{
    /*
     * 创建信息
     *
     * @params $data array
     * @return mix
     */
    public function add($data)
    {
        $validate = Loader::validate('CommentValidate');
        $result = $validate->scene('add')->check($data);

        if(!$result){
            return $validate->getError();
        }

        // 创建时间
        $data['create_at'] = date('Y-m-d H:i:s',time());

        return CommentModel::add($data);
    }

    /*
     * 设置喜欢数量
     *
     * @params $id int 评论ID
     * @params $num int 喜欢数量
     * @return int
     */
    public function setLike($id)
    {
        $num = CommentModel::getLike($id);
        return CommentModel::setLike($id, $num + 1);
    }

}