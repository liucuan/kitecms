<?php
namespace app\admin\controller;

use think\Request;
use app\admin\controller\Admin;
use app\common\model\Section as SectionModel;
use app\common\model\Book as BookModel;

class Section extends Admin
{
    public function index()
    {
        $book_id = Request::instance()->param('id');
        $data['list']= SectionModel::getList(['book_id' => $book_id], 'id desc', 10);
        $data['book'] = BookModel::getInfo($book_id);
        $data['page'] = $data['list']->render();

        $this->assign('data', $data);

        return $this->fetch('admin/section/index');
    }

    public function create()
    {
        $book_id = Request::instance()->param('id');
        $this->assign('book_id', $book_id);
        return $this->fetch('admin/section/create');
    }

    public function store()
    {
        $data = Request::instance()->param();
        $result =  SectionModel::add($data);
        if (is_numeric($result )) {
            return $this->response(200);
        } else {
            return $this->response(201,$result );
        }

    }

    public function edit()
    {
        $id = Request::instance()->param('id');
        $data = SectionModel::getInfo($id);

        $this->assign('data', $data );
        return $this->fetch('admin/section/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();
        $ret = SectionModel::modify($data);

        if (is_numeric($ret)) {
            return $this->response(200);
        } else {
            return $this->response(201,$ret);
        }
    }

    public function remove()
    {
        $id = Request::instance()->param('id');
        $result = SectionModel::remove($id);
        if ($result !== false) {
            return $this->response(200);
        } else {
            return $this->response(100);
        }
    }

}
