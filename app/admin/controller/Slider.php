<?php
namespace app\admin\controller;

use think\Request;
use think\Db;
use app\common\model\Slider as SliderModel;
use app\common\logic\Slider as SliderLogic;

class Slider extends Admin
{

    public function index()
    {
        $logic = new SliderLogic();
        $data = $logic->getList();
        $this->assign('data', $data);
        return $this->fetch('admin/slider/index');
    }

    public function create()
    {
        return $this->fetch('admin/slider/create');
    }

    public function store()
    {
        $data = Request::instance()->param();
        $result = SliderModel::add($data);
        if (is_numeric($result)) {
            return $this->response(200);
        } else {
            return $this->response(201);
        }
    }

    public function edit()
    {
        $id = Request::instance()->param('id');

        $logic = new SliderLogic();
        $data = $logic->getInfo($id);

        $this->assign('data', $data);
        return $this->fetch('admin/slider/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();
        $result = SliderModel::edit($data);
        if (is_numeric($result)) {
            return $this->response(200);
        } else {
            return $this->response(201);
        }
    }

    public function remove()
    {
        if (Request::instance()->isAjax()) {
            $id = Request::instance()->param('id');
            $result = SliderModel::remove($id);
            if (is_numeric($result)) {
                return $this->response(200);
            } else {
                return $this->response(201);
            }
        }
    }

}
