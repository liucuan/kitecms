<?php
namespace app\admin\controller;

use think\Request;
use app\admin\controller\Admin;

class Passport extends Admin
{
    /*
     * 登陆
     *
     */
    public function login()
    {
        return $this->fetch('admin/passport/login');
    }

    /*
     * 登出
     *
     */
    public function logout()
    {
        $ojb = new \app\common\logic\Passport();
        $ojb->clear();
        $this->redirect(url('admin/passport/login'),302);
    }
}
