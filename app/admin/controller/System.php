<?php
namespace app\admin\controller;

use think\Request;
use app\admin\controller\Admin;
use app\common\model\Config as ConfigModel;

class System extends Admin
{
    /*
     * 网站基本信息设置
     *
     */
    public function site()
    {
        // 查询信息
        $data = ConfigModel::getGroupList(1);
        if (!empty($data)) {
            foreach ($data as $v) {
                $this->assign($v['key'], $v['value']);
            }
        }

        // 修改信息
        if (Request::instance()->isAjax()) {
            $request = Request::instance()->param();
            if (!empty($request)) {
                foreach ($request as $k => $v) {
                    $result = ConfigModel::setValue($k, $v);
                }
            }

            if (is_numeric($result)) {
                return $this->response(200);
            } else {
                return $this->response(201);
            }
        }

        return $this->fetch('admin/system/site');
    }

    /*
     * 验证码策略设置
     *
     */
    public function captcha()
    {
        // 查询验证码配置参数
        $data = json_decode(ConfigModel::getValue('captcha'), true);
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $this->assign($k, $v);
            }
        }

        // 查询开启验证码选项
        $open_captcha = ConfigModel::getValue('open_captcha');
        $this->assign('open_captcha', json_decode($open_captcha, true));

        // 修改信息
        if (Request::instance()->isAjax()) {
            $request = Request::instance()->param();

            // 设置开启的验证码选项
            if (!isset($request['open_captcha'])) {
                $request['open_captcha'] = [];
            }
            $resultOne = ConfigModel::setValue('open_captcha', json_encode($request['open_captcha']));
            unset($request['open_captcha']);

            // 验证码生成参数配置
            $resultTwo = ConfigModel::setValue('captcha', json_encode($request));
            if (is_numeric($resultOne) && is_numeric($resultTwo)) {
                return $this->response(200);
            } else {
                return $this->response(201);
            }
        }

        return $this->fetch('admin/system/captcha');
    }

    /*
     * 水印策略设置
     *
     */
    public function water()
    {
        // 查询信息
        $data = json_decode(ConfigModel::getValue('water'), true);
        if (!empty($data['logo'])) {
            $data['logo'] = buildImageUrl($data['logo']);
        }

        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $this->assign($k, $v);
            }
        }

        // 修改信息
        if (Request::instance()->isAjax()) {
            $request = Request::instance()->param();
            $data = json_decode(ConfigModel::getValue('water'), true);
            if (!isset($request['logo'])) {
                $request['logo'] = empty($data['logo']) ? '' : $data['logo'];
            }
            $result = ConfigModel::setValue('water', json_encode($request));

            if (is_numeric($result)) {
                return $this->response(200);
            } else {
                return $this->response(201);
            }
        }

        return $this->fetch('admin/system/water');
    }

    /*
     * 缩略图策略设置
     *
     */
    public function thumb()
    {
        // 修改信息
        if (Request::instance()->isAjax()) {
            $request = Request::instance()->param('position');
            $result = ConfigModel::setValue('thumb', $request);

            if (is_numeric($result)) {
                return $this->response(200);
            } else {
                return $this->response(201);
            }
        }

        // 查询信息
        $position = ConfigModel::getValue('thumb');
        $this->assign('position', $position);
        return $this->fetch('admin/system/thumb');
    }

    /*
     * stmp邮件设置
     *
     */
    public function email()
    {
        // 修改信息
        if (Request::instance()->isAjax()) {
            $request = Request::instance()->param();
            $result = ConfigModel::setValue('mail', json_encode($request));

            if (is_numeric($result)) {
                return $this->response(200);
            } else {
                return $this->response(201);
            }
        }

        // 查询信息
        $email = json_decode(ConfigModel::getValue('mail'), true);
        $this->assign('email', $email);
        return $this->fetch('admin/system/mail');
    }
}
