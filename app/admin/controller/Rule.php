<?php
namespace app\admin\controller;

use think\Request;
use app\admin\controller\Admin;
use app\common\model\AuthRule;

class Rule extends Admin
{
    const SPACE = "\xe3\x80\x80\xe3\x80\x80";

    private $data;

    public function _initialize()
    {
        $this->data = list_for_level(AuthRule::getList(), self::SPACE);
    }

    public function index()
    {
        $this->assign('data', $this->data);
        return $this->fetch('admin/rule/index');
    }

    public function create()
    {
        $this->assign('data', $this->data);
        return $this->fetch('admin/rule/create');
    }

    public function store()
    {
        $data = Request::instance()->param();
        $result = AuthRule::add($data);

        if (is_numeric($result)) {
            return $this->response(200);
        } else {
            return $this->response(222, $result);
        }
    }

    public function edit()
    {
        $id = Request::instance()->param('id');
        $info = AuthRule::get($id);

        $this->assign('info', $info);
        $this->assign('list', $this->data);
        return $this->fetch('admin/rule/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();
// var_dump($data);die;
        $result = AuthRule::edit($data);

        if (is_numeric($result)) {
            return $this->response(200);
        } else {
            return $this->response(222, $result);
        }
    }

    public function remove()
    {
        $id = Request::instance()->param('id');

        // 检查该栏目是否拥有子栏目
        $checkSon = AuthRule::getChild($id);
        if ($checkSon) {
            return $this->response(201);
        }

        // 删除栏目
        $result = AuthRule::remove($id);
        if ($result !== false) {
            return $this->response(200);
        }
        return $this->response(201);
    }

    public function reorder()
    {
        $data = Request::instance()->param();
        $list = array_combine($data['ids'],$data['reorder']);
        foreach($list as $k=>$v){
            $result = AuthRule::reorder($k, $v);
        }

        if ($result !== false) {
            return $this->response(200);
        }

        return $this->response(201);
    }
}
