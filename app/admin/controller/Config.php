<?php
namespace app\admin\controller;

use think\Request;
use app\admin\controller\Admin;
use app\common\model\Config as ConfigModel;
use app\common\logic\Config as ConfigLogic;

class Config extends Admin
{
    protected $configLogic;

    public function _initialize()
    {
        $this->configLogic = new ConfigLogic();
    }

    public function index()
    {
        $data = ConfigModel::getList();
        $this->assign('data', $data);
        return $this->fetch('admin/config/index');
    }

    public function create()
    {
        return $this->fetch('admin/config/create');
    }

    public function store()
    {
        $data = Request::instance()->param();
        $ret = $this->configLogic->add($data);
        if (is_numeric($ret)) {
            return $this->response(200);
        } else {
            return $this->response(201, $ret);
        }
    }

    public function edit()
    {
        $id = Request::instance()->param('id');
        $info = ConfigModel::getInfo($id);
        $this->assign('data', $info);
        return $this->fetch('admin/config/edit');
    }

    public function update()
    {
        $data = Request::instance()->param();
        $ret = $this->configLogic->edit($data);
        if (is_numeric($ret)) {
            return $this->response(200);
        } else {
            return $this->response(201, $ret);
        }
    }

    public function remove()
    {
        if (Request::instance()->isAjax()) {
            $id = Request::instance()->param('id');
            $ret = $this->configLogic->remove($id);
            if (is_numeric($ret)) {
                return $this->response(200);
            } else {
                return $this->response(201, $ret);
            }
        }
    }
}
