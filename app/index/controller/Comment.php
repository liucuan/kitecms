<?php
namespace app\index\controller;

use think\Request;
use app\common\service\Article as ArticleLogic;
use app\common\service\Comment as CommentLogic;
use app\index\controller\Base;

class Comment extends Base
{

    protected $data = [];
    protected $id   = '';

    function _initialize()
    {
        $this->id = $this->getArticleId();
        $this->data = $this->getData();
    }

    protected function getData()
    {
        $articleLogic = new ArticleLogic();
        $articleLogic->setPv($this->id);

        return $articleLogic->getInfo($this->id);
    }

    protected function getArticleId()
    {
        return Request::instance()->param('id');
    }

    public function read()
    {
        // 文章信息
        if (!empty($this->data)) {
            // 数组键名转换为变量 并传值到模板
            foreach ($this->data as $k => $v) {
                $this->assign($k, $v);
            }
        } else {
            $this->error('您访问的页面不存在!');
        }

        // 评论列表
        $commentLogic = new CommentLogic();
        $data = $commentLogic->getList($this->id, 'id desc', $this->config('comment_limit'));

        $this->assign('list', $data['list']);
        $this->assign('page', $data['page']);

        // 模版 优先使用栏目独立模版配置
        return $this->fetch($this->template('article_comment_tpl', $this->data['comment_template']));
    }
}
