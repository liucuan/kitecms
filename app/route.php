<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\Route;
// 注册路由到index模块的News控制器的read操作
// Route::rule('new/:id','admin/index/create');

    // 首页
    Route::rule('/','index/index/index');

    // 登陆登出注册
    Route::rule('login','index/member/login');
    Route::rule('register','index/member/register');
    Route::rule('logout','index/member/logout');

    // 搜索
    Route::rule('search','index/search/index');

    // 文章
    Route::group('article',[
        ':id' => ['index/article/read', ['method' => 'get'], ['id' => '\d+']],
    ]);

    // 评论
    Route::group('comment',[
        ':id' => ['index/comment/read', ['method' => 'get'], ['id' => '\d+']],
    ]);

    // 评论
    Route::group('tag',[
        ':id' => ['index/tag/read', ['method' => 'get'], ['id' => '\d+']],
    ]);

    // 图书
    Route::group('book',[
        ':id' => ['index/book/read', ['method' => 'get'], ['id' => '\d+']],
        'chapter/:id' => ['index/chapter/read', ['method' => 'get'], ['id' => '\d+']],
    ]);

    // 分类
    Route::group('category',[
        ':alias' => ['index/category/read', ['method' => 'get'], ['alias' => '\w+']],
    ]);

    // 会员中心
    Route::group('member',[
        'message' => ['index/member/message', ['method' => 'get']],
        'readmessage/:id' => ['index/member/readmessage', ['method' => 'get'], ['id' => '\d+']],
        'article' => ['index/member/article', ['method' => 'get']],
        'comment' => ['index/member/comment', ['method' => 'get']],
        'favorite' => ['index/member/favorite', ['method' => 'get']],
        'setting' => ['index/member/setting', ['method' => 'get']],
        'profile/:uid' => ['index/member/profile', ['method' => 'get'], ['uid' => '\d+']],
    ]);

    // 后台
    Route::group('admin',[
        'login' => ['admin/passport/login', ['method' => 'get']],
    ]);