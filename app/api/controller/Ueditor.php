<?php
namespace app\api\controller;

use think\Request;
use ueditor\Ueditor as Editor;
use app\api\controller\ApiBase;

class Ueditor extends ApiBase
{
    /**
     * 统一请求地址
     * @return json
     */
    public function index()
    {
        $ojb = new Editor();
        return $ojb->route();
    }
}
