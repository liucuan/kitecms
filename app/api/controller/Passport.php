<?php
namespace app\api\controller;

use think\Request;
use app\api\controller\ApiBase;
use app\common\logic\Passport as PassportLogic;
use app\common\model\Config as ConfigModel;

class Passport extends ApiBase
{
    /**
     * 登陆
     *
     */
    public function login()
    {
        $username = Request::instance()->param('username');
        $password = Request::instance()->param('password');
        $type     = Request::instance()->param('type'); // 1为后台登陆 空为前台登陆

        // 用户名或者密码不能为空
        if (empty($username) || empty($password)) {
            return $this->response(1104);
        }

        // 登陆操作如果开启验证码
        $captchaOption = json_decode(ConfigModel::getValue('open_captcha'), true);

        // 校验验证码
        $is_captcha = in_array($type == 1 ? 5 : 2, $captchaOption);
        if ($is_captcha) {
            $verify_code = Request::instance()->param('verify_code');
            if (empty($verify_code)) {
                return $this->response(1105);
            }
            if(! captcha_check($verify_code)) {
                return $this->response(1106);
            }
        }

        $passportLogic = new PassportLogic();
        $loginData = $passportLogic->login($username, $password);
        return $this->response($loginData);
    }

    /**
     * 登陆
     *
     */
    public function register()
    {
        $requst = Request::instance()->param();

        // 用户名或者密码不能为空
        if (! $requst['username'] || ! $requst['password']) {
            return $this->response(1104);
        }

        // 邮箱不能为空
        if (! $requst['email']) {
            return $this->response(1107);
        }

        // 注册会员
        $passportLogic = new PassportLogic();
        $passportLogic->register();
        var_dump($requst);
    }
}
