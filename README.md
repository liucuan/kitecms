# 关于CMS

KiteCms 是一个基于ThinkPHP5.0.9开发的开源内容管理系统，秉承快速开发和大道至简的核心开发理念。为开发者集成了RBAC（角色管理）的权限管理、文章、图书、配置项等等。模版设计全标签式调用，完善的API数据接口，模型分层设计（数据层-逻辑层-验证层-服务层），代码写作规范清晰，开箱即可使用，开发者快速构建自己的应用。

# 目录结构
~~~
wwwdata  应用部署目录
├─application           应用目录（可设置）
│  ├─common             公共模块目录（可设置）
│  │  ├─behavior      	行为定义文件目录
│  │  ├─controller      公共控制器目录
│  │  ├─model      	模型（数据交互层）目录
│  │  ├─logic      	逻辑模型（数据处理层）目录
│  │  ├─service      	服务模型（服务接口层）目录
│  │  ├─validate      	数据验证模型（数据验证层）目录
│  │  ├─taglib      	自定义模版标签库目录
│  ├─index              前台模块目录（可设置）
│  │  ├─controller      控制器目录
│  ├─admin              后台模块目录（可设置）
│  │  ├─controller      控制器目录
│  ├─api                Api模块目录（可设置）
│  │  ├─controller      控制器目录
│  ├─extra              自定义配置文件目录
│  │  ├─cache         	缓存配置文件
│  │  ├─code         	错误码配置文件
│  │  └─ ...            更多自定义配置文件
│  ├─command.php        命令行工具配置文件
│  ├─common.php         应用公共（函数）文件
│  ├─config.php         应用（公共）配置文件
│  ├─database.php       数据库配置文件
│  ├─tags.php           应用行为扩展定义文件
│  └─route.php          路由配置文件
├─extend                扩展类库目录（可定义）
├─static                静态资源存放目录(css,js,image)
│  ├─adminlte           后台使用的UI框架
│  ├─bootstrap          bootstrap
│  ├─cropbox            头像裁剪组件
│  ├─fileinput          图片上传组件
│  ├─jquery-validation  前端验证组件
│  ├─sweetalert     	消息提示组件
│  └─ ...            	更多静态资源
├─runtime               应用的运行时目录（可写，可设置）
├─vendor                第三方类库目录（Composer）
├─system              	Thinkphp框架系统目录（可设置）
│  ├─lang               语言包目录
│  ├─library            框架核心类库目录
│  │  ├─think           Think 类库包目录
│  │  └─traits          系统 Traits 目录
│  ├─tpl                系统模板目录
│  ├─.htaccess          用于 apache 的重写
│  ├─.travis.yml        CI 定义文件
│  ├─base.php           基础定义文件
│  ├─composer.json      composer 定义文件
│  ├─console.php        控制台入口文件
│  ├─convention.php     惯例配置文件
│  ├─helper.php         助手函数文件（可选）
│  ├─LICENSE.txt        授权说明文件
│  ├─phpunit.xml        单元测试配置文件
│  ├─README.md          README 文件
│  └─start.php          框架引导文件
├─build.php             自动生成定义文件（参考）
├─composer.json         composer 定义文件
├─LICENSE.txt           授权说明文件
├─README.md             README 文件
├─think                 命令行入口文件
├─index.php          	应用入口文 件
├─router.php         	快速测试文件
├─.htaccess          	用于 apache 的重写
~~~

## 命名规范

延续`ThinkPHP5`开发理念，遵循PSR-2命名规范和PSR-4自动加载规范，并且注意如下规范：

### 目录和文件

*   目录不强制规范，驼峰和小写+下划线模式均支持；
*   类库、函数文件统一以`.php`为后缀；
*   类的文件名均以命名空间定义，并且命名空间的路径和类库文件所在路径一致；
*   类名和类文件名保持一致，统一采用驼峰法命名（首字母大写）；

### 函数和类、属性命名
*   类的命名采用驼峰法，并且首字母大写，例如 `User`、`UserType`，默认不需要添加后缀，例如`UserController`应该直接命名为`User`；
*   函数的命名使用小写字母和下划线（小写字母开头）的方式，例如 `get_client_ip`；
*   方法的命名使用驼峰法，并且首字母小写，例如 `getUserName`；
*   属性的命名使用驼峰法，并且首字母小写，例如 `tableName`、`instance`；
*   以双下划线“__”打头的函数或方法作为魔法方法，例如 `__call` 和 `__autoload`；

### 常量和配置
*   常量以大写字母和下划线命名，例如 `APP_PATH`和 `THINK_PATH`；
*   配置参数以小写字母和下划线命名，例如 `url_route_on` 和`url_convert`；

### 数据表和字段
*   数据表和字段采用小写加下划线方式命名，并注意字段名不要以下划线开头，例如 `think_user` 表和 `user_name`字段，不建议使用驼峰和中文作为数据表字段命名。

# 安装使用

1. 导入 `/install/install.sql ` 数据到数据中
2. 修改 `/app/database.php` 填写数据库信息
3. 登陆后台  `/admin/login`

>    默认用户名： admin 默认密码： 123456

# 在线演示

* 前台首页 ：http://edms.kitesky.com
* 后台登陆地址：http://edms.kitesky.com/admin/login
* 用户名：`kite`
* 密码：`123456`

**QQ讨论群: 106938883**

[![](//pub.idqqimg.com/wpa/images/group.png)](//shang.qq.com/wpa/qunwpa?idkey=0bce986ba0fc3c6fba26b6d54e733520ceb5c878737a05caee38dec84717b05c)


# 相关项目
1. ThinkPHP  http://git.oschina.net/liu21st/thinkphp5
2. Bootstrap https://github.com/twbs/bootstrap
3. AdminLTE https://github.com/almasaeed2010/AdminLTE
4. SweetAlert  https://github.com/t4t5/sweetalert
5. Jquery-Validation https://github.com/jquery-validation/jquery-validation